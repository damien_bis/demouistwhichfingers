/*
 * Damien Masson 2017
 *
 * Examples
 *
 * => To save data :
 * LogFile log("test.csv");
 *
 * log.addField("field1", "defaultValue");
 * log.addField("field2", "10");
 * log.addField("field3", "field3default");
 * (...)
 * log.newEntry();
 * log.setValue("field2", 54);
 * (...)
 * log.newEntry();
 * log.setValue("field2", 78);
 * log.setValue("field1", "example");
 * (...)
 * log.save();
 *
 * => To load data
 * LogFile log("test.csv");
 *
 * while (log.nextEntry()) {
 *  log.getString("field1");
 *  log.getInt("field2");
 * }
 *
 *
 * Random ideas :
 * - More types
 * - Automatic column (by using a callback) (timestamp, id...)
 */

#ifndef LOGFILE_H
#define LOGFILE_H

#include <QString>
#include <QHash>
#include <QList>
#include <QStringList>
#include <QFile>
#include <QTextStream>

class LogFile
{
public:
    LogFile(QString filename, bool appendData = true, bool addTimestamp = true, QString separator = ";");
    ~LogFile();

    // Saving
    void addField(QString fieldName, QString defaultValue = QString(""));
    QString setValue(QString fieldName, QString value);

    bool setValue(QString fieldName, bool value);
    long setValue(QString fieldName, long value);
    uint setValue(QString fieldName, uint value);
    int setValue(QString fieldName, int value);
    ulong setValue(QString fieldName, ulong value);
    qlonglong setValue(QString fieldName, qlonglong value);
    qulonglong setValue(QString fieldName, qulonglong value);
    double setValue(QString fieldName, double value);
    QList<QString> setValue(QString fieldName, QList<QString> value);
    QList<int> setValue(QString fieldName, QList<int> value);
    // TODO : More types

    void newEntry();
    void save();

    // Loading
    QString getString(QString fieldName);
    int getInt(QString fieldName);
    bool getBool(QString fieldName);
    QStringList getStrings(QString fieldName);
    QList<int> getInts(QString fieldName);
    // TODO More types

    void reset();
    bool nextEntry();

    void setAutoSave(int nbEntryBeforeSaving);

private:
    QHash<QString, int> fieldId;
    QStringList currentRow;
    QStringList defaultRow;
    QString data;
    int nbFields;
    int nbEntries;
    QString separator;
    QString filename;
    bool currentRowModified;
    bool appendData;
    bool firstRowAlreadyInFile;
    bool addTimestamp;
    int nbEntryBeforeSaving;
    int nbEntryUnsaved;
    // Loading
    void load();
    QFile* loadedFile;
    QTextStream* loadedStream;
    bool reading = false;
};

#endif // LOGFILE_H
