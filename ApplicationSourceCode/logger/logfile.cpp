/*
 * Damien Masson 2017
 */

#include "logfile.h"
#include <QDebug>
#include <QDateTime>

LogFile::LogFile(QString filename, bool appendData, bool addTimestamp, QString separator) {
    nbFields = 0;
    nbEntries = 0;
    this->separator = separator;
    currentRowModified = false;
    this->filename = filename;
    loadedStream = NULL;
    loadedFile = NULL;
    this->appendData = appendData;
    firstRowAlreadyInFile = false;
    this->addTimestamp = addTimestamp;
    this->nbEntryBeforeSaving = -1;
    nbEntryUnsaved = 0;

    if (QFile(filename).exists()) {
        if (!appendData) {
            if (!QFile(filename).remove()) {
                qDebug() << "Unable to replace" << filename;
            }
        } else {
            load();
            reset();
            if (fieldId.size() > 0) {
                firstRowAlreadyInFile = true;
            }
        }
    }

    if (addTimestamp) {
        addField("Timestamp", "");
    }
}

void LogFile::setAutoSave(int nbEntryBeforeSaving) {
    this->nbEntryBeforeSaving = nbEntryBeforeSaving;
}

void LogFile::addField(QString fieldName, QString defaultValue) {
    if (nbEntries > 0) {
        qDebug() << "Cannot add fields after adding entries";
        return;
    }

    if (firstRowAlreadyInFile) {
        if (!fieldId.contains(fieldName)) {
            qDebug() << "Appending data to a csv that had a different format :" << fieldName << "not existing";
            qDebug() << "The old csv will be replaced";
            QFile(filename).remove();
            firstRowAlreadyInFile = false;
        } else {
            defaultRow[fieldId[fieldName]] = defaultValue;
        }
    }

    if (!firstRowAlreadyInFile && !fieldId.contains(fieldName)) {
        currentRow.append(fieldName);
        defaultRow.append(defaultValue);
        fieldId[fieldName] = nbFields++;
        currentRowModified = true;
    }
}

QString LogFile::setValue(QString fieldName, QString value) {
    if (!fieldId.contains(fieldName)) {
        qDebug() << fieldName << "doesn't exists";
        return QString();
    }

    if (nbEntries == 0) {
        qDebug() << "You have to call newEntry() before every new entry";
        return QString();
    }

    currentRowModified = true;
    currentRow[fieldId[fieldName]] = value;

    return value;
}

bool LogFile::setValue(QString fieldName, bool value) {setValue(fieldName, value ? 1 : 0); return value;}
long LogFile::setValue(QString fieldName, long value) {setValue(fieldName, QString::number(value)); return value;}
uint LogFile::setValue(QString fieldName, uint value) {setValue(fieldName, QString::number(value)); return value;}
int LogFile::setValue(QString fieldName, int value) {setValue(fieldName, QString::number(value)); return value;}
ulong LogFile::setValue(QString fieldName, ulong value) {setValue(fieldName, QString::number(value)); return value;}
qlonglong LogFile::setValue(QString fieldName, qlonglong value) {setValue(fieldName, QString::number(value)); return value;}
qulonglong LogFile::setValue(QString fieldName, qulonglong value) {setValue(fieldName, QString::number(value)); return value;}
double LogFile::setValue(QString fieldName, double value) {setValue(fieldName, QString::number(value)); return value;}

QList<QString> LogFile::setValue(QString fieldName, QList<QString> value) {
    setValue(fieldName, QStringList(value).join(","));
    return value;
}

QList<int> LogFile::setValue(QString fieldName, QList<int> value) {
    if (value.isEmpty()) {
        return value;
    }

    QString txt = QString::number(value.at(0));
    for (int i = 1; i < value.size(); ++i) {
        txt += "," + QString::number(value.at(i));
    }
    setValue(fieldName, txt);
    return value;
}

void LogFile::newEntry() {
    reading = false;
    ++nbEntryUnsaved;
    if (!currentRowModified) {
        if (addTimestamp) {
            setValue("Timestamp", QDateTime::currentMSecsSinceEpoch());
        }
        return;
    }
    if (nbEntries > 0 || !firstRowAlreadyInFile) {
        data += currentRow.join(separator) + "\n";
    }
    currentRow = defaultRow;
    ++nbEntries;

    if (addTimestamp) {
        setValue("Timestamp", QDateTime::currentMSecsSinceEpoch());
    }
    currentRowModified = false;

    if (nbEntryBeforeSaving >= 0 && nbEntryUnsaved >= nbEntryBeforeSaving) {
        save();
    }
}

void LogFile::save() {    
    if (currentRowModified) {
        newEntry();
    }

    QFile file(filename);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        QTextStream stream(&file);
        stream << data;
    } else {
        qDebug() << "Unable to write to" << filename << "\n" << data;
    }

    nbEntryUnsaved = 0;

    data.clear();
}

void LogFile::load() {
    nbFields = 0;
    currentRow.clear();
    defaultRow.clear();
    fieldId.clear();
    firstRowAlreadyInFile = false;

    // Load the first line to get the fields
    loadedFile = new QFile(filename);
    if (loadedFile->open(QIODevice::ReadOnly)) {
       loadedStream =  new QTextStream(loadedFile);
       if (!loadedStream->atEnd()) {
          QStringList fields = loadedStream->readLine().split(separator);
          for (int i = 0; i < fields.size(); ++i) {
              addField(fields.at(i), "");
          }
       }
    } else {
        qDebug() << "Unable to open the file" << filename;
    }
}

QString LogFile::getString(QString fieldName) {
    if (loadedStream == NULL || loadedFile == NULL) {
        qDebug() << "You must call nextEntry() before retrieving the values";
        return QString();
    }

    if (!fieldId.contains(fieldName)) {
        qDebug() << fieldName << "not found for file" << filename << "(" << fieldId << ")";
    }

    return currentRow.at(fieldId[fieldName]);
}

int LogFile::getInt(QString fieldName) {
    return getString(fieldName).toInt();
}

bool LogFile::getBool(QString fieldName) {
    return getInt(fieldName) != 0;
}

QStringList LogFile::getStrings(QString fieldName) {
    return getString(fieldName).split(",");
}

QList<int> LogFile::getInts(QString fieldName) {
    QStringList values = getStrings(fieldName);
    QList<int> convValues;
    for (int i = 0; i < values.size(); ++i) {
        convValues.append(values.at(i).toInt());
    }

    return convValues;
}

void LogFile::reset() {
    if (loadedFile != NULL) {
        loadedFile->close();
    }

    loadedStream = NULL;
    loadedFile = NULL;
}

bool LogFile::nextEntry() {
    reading = true;
    if (loadedStream == NULL || loadedFile == NULL) {
        load();
    }
    if (loadedStream->atEnd()) {
        reset();
        return false;
    }

    currentRow = loadedStream->readLine().split(separator);
    return true;
}

LogFile::~LogFile() {
    if (!reading) {
        save();
        reset();
    }
    reading = false;
}
