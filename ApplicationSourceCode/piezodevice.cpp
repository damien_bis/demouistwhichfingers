/* -
 *
 * Author: Damien Masson
 * Copyright University of Lille / Inria
 *
 * http://ns.inria.fr/mjolnir/whichfingers/
 *
 * This software may be used and distributed according to the terms of
 * the GNU General Public License version 2 or any later version.
 *
 */

#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

#include "piezodevice.h"


PiezoDevice::PiezoDevice(unsigned short vendorId, unsigned short productId) {
    handle = NULL;

    hid_init();
    handle = hid_open(vendorId, productId, NULL);

    if (handle == NULL) {
        qDebug() << "Error, unable to open Piezo device";
        return;
    }

    hid_set_nonblocking(handle, 0);

    PiezoDeviceUpdateThread *updateThread = new PiezoDeviceUpdateThread(this);
    updateThread->connect(updateThread, SIGNAL(finished()), updateThread, SLOT(deleteLater()));
    updateThread->start();
}

QQueue<double> PiezoDevice::getPiezoValues(int piezoId) {
    return piezoValues[piezoId];
}

void PiezoDevice::update() {
    char buff[6];
    hid_read_timeout(handle, (unsigned char*) &buff, sizeof(buff), -1);

    for (int j = 0; j < NB_PIEZO; ++j) {
        piezoValues[j].append(buff[j + 1]);
        if (piezoValues[j].size() > HISTORY_SIZE) {
            piezoValues[j].dequeue();
        }
    }
}
