#include "touchpadevent.h"

TouchpadEvent::TouchpadEvent(int id, TouchType type, double x, double y) :
    QEvent(eventType),
    id(id), type(type), x(x), y(y) {
    
}


double TouchpadEvent::getX() {
    return x;
}

double TouchpadEvent::getY() {
    return y;
}

int TouchpadEvent::getId() {
    return id;
}

TouchpadEvent::TouchType TouchpadEvent::getType() {
    return type;
}

