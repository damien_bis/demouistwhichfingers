#ifndef TOUCHPADEVENT_H
#define TOUCHPADEVENT_H

#include <QEvent>

class TouchpadEvent : public QEvent
{
public:
    static const QEvent::Type eventType = static_cast<QEvent::Type>(4242);
    enum TouchType {
        TouchPress,
        TouchRelease
    };
    
    TouchpadEvent(int id, TouchType type, double x, double y);
    
    double getX();
    double getY();
    int getId();
    TouchType getType();
    
    
private:
    int id;
    double x;
    double y;
    TouchType type;
};

#endif // TOUCHPADEVENT_H
