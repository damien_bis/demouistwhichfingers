/* -
 *
 * Author: Damien Masson
 * Copyright University of Lille / Inria
 *
 * http://ns.inria.fr/mjolnir/whichfingers/
 *
 * This software may be used and distributed according to the terms of
 * the GNU General Public License version 2 or any later version.
 *
 */

#ifndef PIEZODEVICE_H
#define PIEZODEVICE_H

#include <hidapi/hidapi.h>
#include <QQueue>

#define NB_PIEZO 5
#define HISTORY_SIZE 500 // How many values per piezo should we keep

class PiezoDevice
{
public:
    PiezoDevice(unsigned short vendorId, unsigned short productId);
    void update();
    QQueue<double> getPiezoValues(int piezoId);

private:
    hid_device* handle;
    bool connected;
    QQueue<double> piezoValues[NB_PIEZO];
};


#include <QThread>

class PiezoDeviceUpdateThread : public QThread
{
    Q_OBJECT

public:
    PiezoDeviceUpdateThread(PiezoDevice* device) {
        this->device = device;
    }

    void run() {
        while (1) {
            device->update();
        }
    }

private:
    PiezoDevice* device;
};

#endif // PIEZODEVICE_H
