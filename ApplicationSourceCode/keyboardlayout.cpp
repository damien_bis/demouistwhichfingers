#include "keyboardlayout.h"
#include <QDebug>
#include <QKeySequence>
#include <QKeyEvent>
#include "logger/logfile.h"

Key::Key(QString keyName, QString text, int keyCode, QPoint pos, int finger) :
    Key(keyName, text, keyCode, pos, 32, finger) {
    logged = false;
}

Key::Key(QString keyName, QString text, int keyCode, QPoint pos, int width, int finger) :
    keyName(keyName),
    text(text),
    keyCode(keyCode),
    pos(pos),
    width(width) {
    fingers.append(finger);
}

void Key::addFinger(int finger) {
    fingers.append(finger);
}

Key* Key::addShiftText(QString txt) {
    shiftText = txt;
    return this;
}

bool Key::isLogged() {
    return logged;
}

Key* Key::setLogged(bool logged) {
    this->logged = logged;
    return this;
}

Key* Key::addAltGrText(QString txt) {
    altGrText = txt;
    return this;
}

bool Key::isCorrectFinger(int finger) {
    fingers.contains(finger);
}

Key* KeyboardLayout::getKey(int col, int row) {
    return keysArray[row][col];
}

void KeyboardLayout::newRow() {
    ++currRowId;
    currColId = 0;
}

Key* KeyboardLayout::addKey(int row, int col, QString keyName, QString text, int keyCode, QPoint pos, int width, int finger) {
    return keysArray[row][col] = keys[text] = (new Key(keyName, text, keyCode, pos, width, finger));
}

Key* KeyboardLayout::addKey(int row, int col, QString keyName, QString text, int keyCode, QPoint pos, int finger) {
    return keysArray[row][col] = keys[text] = (new Key(keyName, text, keyCode, pos, finger));
}

KeyboardLayout::KeyboardLayout() {
    LogFile f("../AZERTY.csv", true, false, "\t");
    
    while (f.nextEntry()) {
        QList<int> fingers = f.getInts("Fingers");
        QString keyCodeField = "QtKeycode";
        int row = f.getInt("Row");
        int col = f.getInt("Col");
        Key* k = addKey(row, col, f.getString("Key"), f.getString("Text"), f.getInt(keyCodeField), QPoint(0, 0), f.getInt("Width"), fingers.at(0));        
        k->setLogged(f.getBool("Logged"));
    }
}

Key* KeyboardLayout::getKey(QString keyText) {
    Key* key  = keys[keyText];

    if (key == NULL) {
        // Maybe the key is done using a modifier
        QHash<QString, Key*>::iterator i;
        for (i = keys.begin(); i != keys.end(); ++i) {
            Key* k = i.value();
            if (k != NULL) {
                if (!k->shiftText.isEmpty() && k->shiftText == keyText) {
                    return k;
                } else if (!k->altGrText.isEmpty() && k->altGrText == keyText) {
                    return k;
                }
            }
        }
    }

    return key;
}

Key* KeyboardLayout::getKey(int keycode) {
    QHash<QString, Key*>::iterator i;
    for (i = keys.begin(); i != keys.end(); ++i) {
        Key* k = i.value();
        if (k != NULL) {
            if (k->keyCode == keycode) {
                return k;
            }
        }
    }
    return NULL;
}

QHash<QString, Key*> KeyboardLayout::getKeys() {
    return keys;
}
