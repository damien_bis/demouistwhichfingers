
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WhichFingersDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    piezodevice.cpp \
    keyboardlayout.cpp \
    logger/logfile.cpp \
    events/touchpadevent.cpp

HEADERS  += mainwindow.h \
    piezodevice.h \
    keyboardlayout.h \
    logger/logfile.h \
    os_specific/os_specific.h \
    events/touchpadevent.h

FORMS    += mainwindow.ui

windows {

  message("Needs to be configured")

} else:mac {

    LIBS += -F/System/Library/PrivateFrameworks \
            -framework MultitouchSupport -v \
            -framework AppKit
    
    #OBJECTIVE_SOURCES += 


    SOURCES += os_specific/mac/mac.cpp

    HEADERS += os_specific/mac/osxPrivateMultitouchSupport.h


  # Look for pkg-config in Fink, Macports and Homebrew (the last one we find wins)
  exists(/sw/bin/pkg-config) {
      QMAKE_PKG_CONFIG = /sw/bin/pkg-config
      INCLUDEPATH += /sw/include            
  }
  exists(/opt/local/bin/pkg-config) {
	  QMAKE_PKG_CONFIG = /opt/local/bin/pkg-CONFIG
      INCLUDEPATH += /opt/local/include      
  }
  exists(/usr/local/bin/pkg-config) {
      QMAKE_PKG_CONFIG = /usr/local/bin/pkg-config
      INCLUDEPATH += /usr/local/include
  }

  message("Looking for dependencies using $$QMAKE_PKG_CONFIG")

  system($$QMAKE_PKG_CONFIG --exists hidapi) {
    QMAKE_CXXFLAGS += $$system("$$QMAKE_PKG_CONFIG --cflags hidapi")
    LIBS += $$system("$$QMAKE_PKG_CONFIG --libs hidapi")
    DEFINES += LIBLAG_HAS_HIDAPI
    CONFIG += hidapi
    message("HIDAPI support enabled")	
  }

} else:linux-*  {

    SOURCES +=     os_specific/linux/touchpad/touchpad.cpp \
                   os_specific/linux/linux.cpp \
                   os_specific/linux/touchpad/evtest.c

    HEADERS +=     os_specific/linux/touchpad/evtest.h \
                   os_specific/linux/touchpad/touchpad.h
    
  # Look for pkg-config in the usual places
  exists(/usr/bin/pkg-config) { QMAKE_PKG_CONFIG = /usr/bin/pkg-config }
  exists(/usr/local/bin/pkg-config) { QMAKE_PKG_CONFIG = /usr/local/bin/pkg-config }
  message("Looking for dependencies using $$QMAKE_PKG_CONFIG")

  # Use hidapi-libusb, not hidapi-raw (which does not work with some devices)
  # See https://github.com/signal11/hidapi
  system($$QMAKE_PKG_CONFIG --exists hidapi-libusb) {
    QMAKE_CXXFLAGS += $$system("$$QMAKE_PKG_CONFIG --cflags hidapi-libusb")
    LIBS += $$system("$$QMAKE_PKG_CONFIG --libs hidapi-libusb")
    DEFINES += LIBLAG_HAS_HIDAPI
    CONFIG += hidapi
    message("HIDAPI support enabled")	
  }

}
