/* -
 *
 * Author: Damien Masson
 * Copyright University of Lille / Inria
 *
 * http://ns.inria.fr/mjolnir/whichfingers/
 *
 * This software may be used and distributed according to the terms of
 * the GNU General Public License version 2 or any later version.
 *
 */

#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include "os_specific/os_specific.h"
#include "events/touchpadevent.h"

QObject* eventReceiver = NULL;

void onTouchpadEvent(unsigned long timestamp, double x, double y, int slot, bool removed) {
    TouchpadEvent* event = new TouchpadEvent(slot, removed ? TouchpadEvent::TouchRelease : TouchpadEvent::TouchPress, x, y);

    QApplication::postEvent(eventReceiver, event);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    eventReceiver = &w;
    w.show();
    
    installTouchpadCallback();
    int res = a.exec();
    removeTouchpadCallback();
    return res;
}
