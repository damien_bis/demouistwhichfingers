/* -
 *
 * Author: Damien Masson
 * Copyright University of Lille / Inria
 *
 * http://ns.inria.fr/mjolnir/whichfingers/
 *
 * This software may be used and distributed according to the terms of
 * the GNU General Public License version 2 or any later version.
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "keyboardlayout.h"
#include "piezodevice.h"

enum Device {
    Keyboard,
    Touchpad
};

typedef struct _finger {
    int touchId;
    double x;
    double y;
    Device device;
    unsigned long timestamp;
    bool down;
} finger;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void keyPressEvent(QKeyEvent * event);
    void keyReleaseEvent(QKeyEvent * event);
    void drawKeyboardLayout();
    void drawTouchpadView();
    void updateCheckboxes();
    int getFingerUsed();
    bool event(QEvent *event);
    int getFingerAssociated(Device device, int touchId, bool down);
    

    finger fingerKey[5];

    Ui::MainWindow *ui;
    PiezoDevice device;
    KeyboardLayout layout;
    
    const char* fingerNames[5] = {"Little", "Ring", "Middle", "Index", "Thumb"};
    QColor* fingerColors[5] = {new QColor("#e41a1c"), new QColor("#377eb8"), new QColor("#4daf4a"), new QColor("#984ea3"), new QColor("#ff7f00")};
};

#endif // MAINWINDOW_H
