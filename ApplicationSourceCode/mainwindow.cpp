/* -
 *
 * Author: Damien Masson
 * Copyright University of Lille / Inria
 *
 * http://ns.inria.fr/mjolnir/whichfingers/
 *
 * This software may be used and distributed according to the terms of
 * the GNU General Public License version 2 or any later version.
 *
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "events/touchpadevent.h"
#include <QKeyEvent>
#include <QPainter>
#include <QDateTime>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    device(0x2341, 0x804e) {
    ui->setupUi(this);
    //ui->buttonGroup->setExclusive(false);

    for (int i = 0; i < 5; ++i) {
        memset(fingerKey + i, 0, sizeof(finger));
    }
    
    drawKeyboardLayout();
    drawTouchpadView();
}

/*
 * Finger identification : look for the piezo with the highest value between t-32 and t-8
 * The timewindow could be different depending on the device your using
 */
int MainWindow::getFingerUsed() {
    double highestVal = 0;
    double fingerUsed = 0;
    for (int j = 0; j < 5; ++j) {
        double delta = QDateTime::currentMSecsSinceEpoch() - fingerKey[j].timestamp;
        if (!fingerKey[j].down && delta > 64) {
            QQueue<double> values = device.getPiezoValues(j);
            for (int i = 8; i < 32; ++i) {
                double val = values.at(values.size() - i);
                if (val > highestVal) {
                    highestVal = val;
                    fingerUsed = j;
                }
            }
        }
    }

    return fingerUsed;
}

void MainWindow::keyPressEvent(QKeyEvent * event) {
    if (!event->isAutoRepeat()) {
        int fingerUsed = getFingerUsed();
        
        fingerKey[fingerUsed].device = Keyboard;
        fingerKey[fingerUsed].down = true;
        fingerKey[fingerUsed].timestamp = QDateTime::currentMSecsSinceEpoch();
        fingerKey[fingerUsed].touchId = event->key();
        fingerKey[fingerUsed].x = 0;
        fingerKey[fingerUsed].y = 0;
        
        drawKeyboardLayout();
    }
    /*
    if (!event->isAutoRepeat()) {
        fingerKey[getFingerUsed()] = event->key();
        updateCheckboxes();
    }*/
}

void MainWindow::keyReleaseEvent(QKeyEvent * event) {
    for (int i = 0; i < 5; ++i) {
        if (fingerKey[i].down && fingerKey[i].device == Keyboard && fingerKey[i].touchId == event->key()) {
            fingerKey[i].down = false;
            fingerKey[i].timestamp = QDateTime::currentMSecsSinceEpoch();
        }
    }
    
    drawKeyboardLayout();
    updateCheckboxes();
}

int MainWindow::getFingerAssociated(Device device, int touchId, bool down) {
    for (int i  = 0; i < 5; ++i) {
        if (fingerKey[i].down == down && fingerKey[i].device == device && fingerKey[i].touchId == touchId) {
            return i;
        }
    }
    
    return -1;
}

bool MainWindow::event(QEvent *event) {
    if (event->type() == TouchpadEvent::eventType) {
        TouchpadEvent* touch = (TouchpadEvent*) event;
        
        int finger = getFingerAssociated(Touchpad, touch->getId(), true);
        
        if (touch->getType() == TouchpadEvent::TouchPress) {
            if (finger < 0) {
                // Touch press
                finger = getFingerUsed();
                fingerKey[finger].device = Touchpad;
                fingerKey[finger].down = true;
                fingerKey[finger].touchId = touch->getId();
            }
          
            fingerKey[finger].timestamp = QDateTime::currentMSecsSinceEpoch();
            fingerKey[finger].x = touch->getX();
            fingerKey[finger].y = touch->getY();
        } else {
            // Touch release
            if (finger >= 0) {
                fingerKey[finger].timestamp = QDateTime::currentMSecsSinceEpoch();
                fingerKey[finger].down = false;
            }
        }
        
        drawTouchpadView();
    }
    
    return QMainWindow::event(event);
}

void MainWindow::updateCheckboxes() {
    for (int i = 0; i < 5; ++i) {
        //ui->buttonGroup->buttons().at(i)->setChecked(fingerKey[i] > 0);
    }
}



void MainWindow::drawKeyboardLayout() {
    QPixmap pixmap(550, 200);
    pixmap.fill(Qt::transparent);
    QImage tmp = pixmap.toImage();
    QPainter painter(&tmp);
    
    for (int i = 0; i < 5; ++i) {
        painter.setBrush(*fingerColors[i]);
        painter.drawEllipse((i > 4 ? 10 : 0) + i * 10, 5, 10, 10);
    }

    // Draw the keyboard
    int offx = 10;
    int offy = 20;
    for (int i = 0; i < 5; ++i) {
        int posx = 0;
        for (int j = 0; j < 15; ++j) {
            Key* layoutKey = layout.getKey(j, i);

            if (layoutKey != NULL) {
                if (layoutKey->keyCode > 0) {
                    painter.setBrush(QColor(255, 255, 255, 255));
                    painter.setPen(QColor(0, 0, 0));
                    painter.drawRect(posx + offx, offy + i * 36, layoutKey->width, 32);
                    for (int f = 0; f < 5; ++f) {
                        int keyCode = fingerKey[f].touchId;
                        if (fingerKey[f].down && keyCode == layoutKey->keyCode) {
                            painter.setPen(QColor(0, 0, 0, 0));
                            painter.setBrush(*fingerColors[f]);
                            painter.drawRect(posx + offx + 1, offy + i * 36 + 1, layoutKey->width - 1, 31);
                        }
                    }

                    painter.setPen(QColor(0, 0, 0, 255));
                    painter.drawText(posx + offx + 15, offy + i * 36 + 15, layoutKey->keyName);
                }
                posx += layoutKey->width + 4;
            }
        }
    }
    painter.end();
    
    ui->keyboardViewLabel->setAlignment(Qt::AlignCenter);
    ui->keyboardViewLabel->setPixmap(QPixmap::fromImage(tmp));
}

void MainWindow::drawTouchpadView() {
    QPixmap pixmap(300, 200);
    pixmap.fill(Qt::transparent);
    QImage tmp = pixmap.toImage();
    QPainter painter(&tmp);
    
    for (int i = 0; i < 5; ++i) {
        if (fingerKey[i].down && fingerKey[i].device == Touchpad) {
            painter.setBrush(*fingerColors[i]);
            
            double radius = pixmap.width() * 0.08;
            double tx = fingerKey[i].x * pixmap.width() - radius / 2;
            double ty = fingerKey[i].y * pixmap.height() - radius / 2;
            
            painter.drawEllipse(tx, ty, radius, radius);
            
            int textWidth = painter.fontMetrics().width(fingerNames[i]);
            int textHeight = painter.fontMetrics().height();
            painter.drawText(tx + (radius - textWidth) / 2, ty + radius + textHeight, fingerNames[i]);
        }
    }
    
    painter.setBrush(Qt::transparent);
    painter.setPen(QColor(0, 0, 0));
    painter.drawRoundedRect(0, 0, pixmap.width() - 1, pixmap.height() - 1, 5, 5);

    painter.end();
    
    ui->trackpadViewLabel->setAlignment(Qt::AlignCenter);
    ui->trackpadViewLabel->setPixmap(QPixmap::fromImage(tmp));
}

MainWindow::~MainWindow() {
    delete ui;
}
