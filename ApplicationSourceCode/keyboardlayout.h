#ifndef KEYBOARDLAYOUT_H
#define KEYBOARDLAYOUT_H

#include <QString>
#include <QPoint>
#include <QHash>

class Key
{
public:
    Key(QString keyName, QString text, int keyCode, QPoint pos, int finger);
    Key(QString keyName, QString text, int keyCode, QPoint pos, int width, int finger);
    bool isCorrectFinger(int finger);
    void addFinger(int finger);
    Key* addShiftText(QString txt);
    Key* addAltGrText(QString txt);
    Key* setLogged(bool logged);
    bool isLogged();

    QString keyName;
    QString text;
    int keyCode;
    QPoint pos;
    int width;
    QList<int> fingers;
    QString shiftText;
    QString altGrText;
    bool logged;


};

class KeyboardLayout
{
public:
    KeyboardLayout();
    Key* getKey(QString keyText);
    Key* getKey(int col, int row);
    Key* getKey(int keycode);
    QHash<QString, Key*> getKeys();

private:
    QHash<QString, Key*> keys;
    Key* keysArray[5][15] = {{NULL}};

    void newRow();
    Key* addKey(int row, int col, QString keyName, QString text, int keyCode, QPoint pos, int finger);
    Key* addKey(int row, int col, QString keyName, QString text, int keyCode, QPoint pos, int width, int finger);
    int currRowId = 0;
    int currColId = 0;
};

#endif // KEYBOARDLAYOUT_H
