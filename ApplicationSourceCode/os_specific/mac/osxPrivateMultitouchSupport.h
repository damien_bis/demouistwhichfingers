/* -*- mode: c++ -*-
 *
 * tIO/utils/osxPrivateMultitouchSupport.h --
 *
 * Authors: Nicolas Roussel
 *
 * Copyright © Inria
 *
 */

#ifndef tIO_utils_osxPrivateMultitouchSupport_h
#define tIO_utils_osxPrivateMultitouchSupport_h

#include <CoreFoundation/CoreFoundation.h>

namespace tIO {

  extern "C" {

    typedef void *MTDeviceRef ;

    typedef struct {
      int i ;
    } MTOneInt ;

    typedef struct {
      int i1 ;
      int i2 ;
    } MTTwoInts ;
  
    typedef struct {
      float x ;
      float y ;
    } MTPoint ;
  
    typedef struct {
      MTPoint pos ;
      MTPoint vel ;
    } MTReadout ;
  
    typedef struct {
      int frame ;
      double timestamp ;
      int identifier ;
      int pathstage ;
      int F ;
      int H ;
      MTReadout normalized ;
      float size ;
      float force ;
      float angle ;
      float majorAxis ;
      float minorAxis ; 
      MTReadout mm ;
      MTTwoInts zero2 ;
      float zden ;
    } MTTouch ;

    typedef enum {
      NotTracking,
      StartInRange,
      HoverInRange,
      MakeTouch,
      Touching,
      BreakTouch,
      LingerInRange,
      OutOfRange
    } MTPathStage ;

    CFMutableArrayRef MTDeviceCreateList(void) ;

    MTDeviceRef MTDeviceCreateDefault(void) ;
    MTDeviceRef MTDeviceCreateFromGUID(UInt64 guid) ;

    void MTDeviceGetGUID(MTDeviceRef device, UInt64 *guid) ;

    int MTDeviceGetSensorDimensions(MTDeviceRef device, int *rows, int *cols) ;
    int MTDeviceGetSensorSurfaceDimensions(MTDeviceRef device, int *width, int *height) ;

    void MTEasyInstallPrintCallbacks(MTDeviceRef device, int unknown_arg) ;

    typedef int (*MTContactFrameCallback)(MTDeviceRef device, 
					  MTTouch *contacts, int numcontacts, 
					  double timestamp, int frame) ;
    void MTRegisterContactFrameCallback(MTDeviceRef device, MTContactFrameCallback callback) ;
    void MTUnregisterContactFrameCallback(MTDeviceRef device, MTContactFrameCallback callback) ;

    typedef int (*MTButtonStateCallback)(MTDeviceRef device, 
					 int pressed, int released) ;
    void MTRegisterButtonStateCallback(MTDeviceRef device, MTButtonStateCallback callback) ;
    void MTUnregisterButtonStateCallback(MTDeviceRef device, MTButtonStateCallback callback) ;

    char *MTGetPathStageName(int pathstage) ;

    void MTDeviceStart(MTDeviceRef device, int unknown_arg) ;
    void MTDeviceStop(MTDeviceRef device) ;

    void MTDeviceRelease(MTDeviceRef device) ;

  }

}

#endif
