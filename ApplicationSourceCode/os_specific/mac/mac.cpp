#include "os_specific/os_specific.h"
#include "os_specific/mac/osxPrivateMultitouchSupport.h"

/*
#include <AppKit/AppKit.h>
#import <Cocoa/Cocoa.h>
#import <Carbon/Carbon.h>
#import "osutils.h"
*/

int MTContactFrameCallback(tIO::MTDeviceRef, tIO::MTTouch *contacts, int numcontacts, double timestamp, int) {
    if (!numcontacts) {
        return 0;
    }
    for (int k = 0; k < numcontacts; k++) {
        
        double tx = contacts[k].normalized.pos.x;
        double ty = 1.0 - contacts[k].normalized.pos.y;
        int id = contacts[k].identifier;
        
        if (contacts[k].pathstage == tIO::MakeTouch || contacts[k].pathstage == tIO::Touching) {
            onTouchpadEvent(timestamp, tx, ty, id, false);
        } else if (contacts[k].pathstage == tIO::OutOfRange) {
            onTouchpadEvent(timestamp, tx, ty, id, true);
        }
    }
}

// Get the id (GUID) of one of the trackpad connected
// An "Apple Magic Mouse" is considered as a trackpad too
// If there's more than one trackpad detected, it will choose the one with the biggest surface
uint64_t trackpadId = 0;
int getTrackpadId() {
    if (!trackpadId) {
        uint64_t guid;
        double maxSize = 0;
        CFMutableArrayRef devices = tIO::MTDeviceCreateList();
        for (int i = 0; i < CFArrayGetCount(devices); ++i) {
            tIO::MTDeviceRef device = (tIO::MTDeviceRef) CFArrayGetValueAtIndex(devices, i);

            tIO::MTDeviceGetGUID(device, &guid);
            int width, height;
            tIO::MTDeviceGetSensorSurfaceDimensions(device, &width, &height);
            double size = ((double)width)/height;
            if (size > maxSize) {
                maxSize = size;
                trackpadId = guid;
            }
        }
    }

    return trackpadId;
}


void installTouchpadCallback() {
    tIO::MTDeviceRef device = tIO::MTDeviceCreateFromGUID(getTrackpadId());

    UInt64 guid;
    tIO::MTDeviceGetGUID(device, &guid);

    int res,  rows, cols, width, height;
    res = tIO::MTDeviceGetSensorDimensions(device, &rows, &cols);
    res = tIO::MTDeviceGetSensorSurfaceDimensions(device, &width, &height);
    
    tIO::MTRegisterContactFrameCallback(device, MTContactFrameCallback);
    tIO::MTDeviceStart(device,0);
}

void removeTouchpadCallback() {
    
}
