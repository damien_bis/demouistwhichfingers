#ifndef OS_SPECIFIC_H
#define OS_SPECIFIC_H


void onTouchpadEvent(unsigned long timestamp, double x, double y, int slot, bool removed);

void installTouchpadCallback();
void removeTouchpadCallback();

#endif // OS_SPECIFIC_H

