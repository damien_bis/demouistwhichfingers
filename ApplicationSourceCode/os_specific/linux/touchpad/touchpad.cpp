/*
 * Damien Masson - 2017
*/
#include <unistd.h>
#include <stdio.h>

#include "touchpad.h"


extern "C" {
    #include "evtest.h"
}
#include <QDebug>
void touchEventsCallback(void *data, long tvsec, long tvusec, int x, int y, int slot, int removed) {
    Touchpad* touchpad = (Touchpad*) data;

    if (touchpad->callback == NULL) {
        return;
    }

    unsigned long timestamp = tvsec * 1000 + tvusec / 1000;

    if (removed) {
        touchpad->callback(timestamp, 0, 0, slot, true);
    } else {

        double x_range = touchpad->xmax - touchpad->xmin;
        double y_range = touchpad->ymax - touchpad->ymin;

        double x_normalized = (x - touchpad->xmin) / x_range;
        double y_normalized = (y - touchpad->ymin) / y_range;

        touchpad->callback(timestamp, x_normalized, y_normalized, slot, false);
    }
}

Touchpad::Touchpad(unsigned short vendorId, unsigned short productId) {
    this->fd = get_device(vendorId, productId);
    this->callback = NULL;
    this->listening_thread = NULL;

    if (!fd) {
        fprintf(stderr, "Unable to open the specified touchpad device\n");
    } else {
        get_touchpad_info(fd, &xmin, &xmax, &ymin, &ymax);
    }
}

void Touchpad::startListening(touchpadCallback callback) {
    this->callback = callback;
    listener_params.fd = fd;
    listener_params.callback = touchEventsCallback;
    listener_params.data = this;



    if (pthread_create(&listening_thread, NULL, (void* (*)(void*)) listen_touchpad_events, ((void*) &listener_params))) {
        fprintf(stderr, "Unable to start a thread reading the touchpad events\n");
    }

}

void Touchpad::stopListening() {
    if (this->listening_thread != NULL) {
        pthread_cancel(listening_thread);
        listening_thread = NULL;
    }

    this->callback = NULL;
}

Touchpad::~Touchpad() {
    if (fd) {
        close(fd);
    }
}

