/*
 *  Read /dev/input/* to obtain touchpad events
 *  Damien Masson 2017
 *  Some part of this code are extracted from the program "evtest", see copyrights below
 *
 *  Copyright (c) 1999-2000 Vojtech Pavlik
 *  Copyright (c) 2009-2011 Red Hat, Inc
 *
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Should you need to contact me, the author, you can do so either by
 * e-mail - mail your message to <vojtech@ucw.cz>, or by paper mail:
 * Vojtech Pavlik, Simunkova 1594, Prague 8, 182 00 Czech Republic
 */
#include "evtest.h"


#define _GNU_SOURCE
#include <stdio.h>
#include <linux/input.h>

#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>

#define BITS_PER_LONG (sizeof(long) * 8)
#define NBITS(x) ((((x)-1)/BITS_PER_LONG)+1)
#define OFF(x)  ((x)%BITS_PER_LONG)
#define BIT(x)  (1UL<<OFF(x))
#define LONG(x) ((x)/BITS_PER_LONG)
#define test_bit(bit, array)	((array[LONG(bit)] >> OFF(bit)) & 1)
#define DEV_INPUT_EVENT "/dev/input"
#define EVENT_DEV_NAME "event"



/**
 * Filter for the AutoDevProbe scandir on /dev/input.
 *
 * @param dir The current directory entry provided by scandir.
 *
 * @return Non-zero if the given directory entry starts with "event", or zero
 * otherwise.
 */
static int is_event_device(const struct dirent *dir) {
    return strncmp(EVENT_DEV_NAME, dir->d_name, 5) == 0;
}


int get_device(unsigned short vendorId, unsigned short productId)
{
    struct dirent **namelist;
    int i, ndev;
    unsigned short id[4];


    ndev = scandir(DEV_INPUT_EVENT, &namelist, is_event_device, versionsort);
    if (ndev <= 0)
        return 0;

    for (i = 0; i < ndev; i++)
    {
        char fname[64];
        int fd = -1;

        snprintf(fname, sizeof(fname),
             "%s/%s", DEV_INPUT_EVENT, namelist[i]->d_name);
        fd = open(fname, O_RDONLY);
        if (fd < 0)
            continue;

        ioctl(fd, EVIOCGID, id);

        if (id[ID_VENDOR] == vendorId && id[ID_PRODUCT] == productId) {
            for (int j = i; j < ndev; ++j) {
                free(namelist[j]);
            }

            return fd;
        }

        close(fd);

        free(namelist[i]);
    }

    return 0;
}

void get_touchpad_info(int fd, int* xmin, int* xmax, int* ymin, int* ymax) {
    unsigned int type, code;
    unsigned long bit[EV_MAX][NBITS(KEY_MAX)];

    memset(bit, 0, sizeof(bit));
    ioctl(fd, EVIOCGBIT(0, EV_MAX), bit[0]);

    for (type = 0; type < EV_MAX; type++) {
        if (test_bit(type, bit[0]) && type != EV_REP) {
            if (type == EV_SYN) continue;

            ioctl(fd, EVIOCGBIT(type, KEY_MAX), bit[type]);
            for (code = 0; code < KEY_MAX; code++)
                if (test_bit(code, bit[type])) {
                    if (type == EV_ABS) {
                        int abs[6] = {0};

                        ioctl(fd, EVIOCGABS(code), abs);

                        if (code == ABS_X) {
                            *xmin = abs[1];
                            *xmax = abs[2];
                        } else if (code == ABS_Y) {
                            *ymin = abs[1];
                            *ymax = abs[2];
                        }
                    }
                }
        }
    }
}

struct _point {
    int x;
    int y;
};

int listen_touchpad_events(listen_touchpad_params* params) {
    struct input_event ev[64];
    int i, rd;
    fd_set rdfs;
    int last_slot = 0;
    int moved = 0;
    
    struct _point touch_points[256];
    

    FD_ZERO(&rdfs);
    FD_SET(params->fd, &rdfs);

    while (1) {
        select(params->fd + 1, &rdfs, NULL, NULL, NULL);

        rd = read(params->fd, ev, sizeof(ev));

        if (rd < (int) sizeof(struct input_event)) {
            printf("expected %d bytes, got %d\n", (int) sizeof(struct input_event), rd);
            perror("\nevtest: error reading");
            return 1;
        }

        for (i = 0; i < rd / ((int) sizeof(struct input_event)); i++) {
            unsigned int type, code;

            type = ev[i].type;
            code = ev[i].code;
            if (type == EV_ABS) {
                if (code == ABS_MT_SLOT) {
                    last_slot = ev[i].value;
                } else if (last_slot >= 0) {
                    
                    if (code == ABS_MT_POSITION_X) {
                        touch_points[last_slot].x =  ev[i].value;
                        moved = 1;
                    } else if (code == ABS_MT_POSITION_Y) {
                        touch_points[last_slot].y =  ev[i].value;
                        moved = 1;
                    } 
                    if (code == ABS_MT_TRACKING_ID && ev[i].value == -1) {
                        params->callback(params->data, ev[i].time.tv_sec, ev[i].time.tv_usec, 0, 0, last_slot, 1);
                    } else if (moved) {
                        moved = 0;
                        params->callback(params->data, ev[i].time.tv_sec, ev[i].time.tv_usec, touch_points[last_slot].x, touch_points[last_slot].y, last_slot, 0);
                    }
                }
            }
        }

    }

    ioctl(params->fd, EVIOCGRAB, (void*)0);
    return EXIT_SUCCESS;
}
