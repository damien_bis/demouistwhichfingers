/*
 * Damien Masson - 2017
*/
#ifndef TOUCHPAD_H
#define TOUCHPAD_H

#include <pthread.h>

extern "C" {
    #include "evtest.h"
}

typedef void (*touchpadCallback)(unsigned long timestamp, double x, double y, int slot, bool removed);

class Touchpad
{
public:
    Touchpad(unsigned short vendorId, unsigned short productId);
    void startListening(touchpadCallback callback);
    void stopListening();
    ~Touchpad();

    int xmin, xmax, ymin, ymax;
    touchpadCallback callback;

private:
    int fd;
    listen_touchpad_params listener_params;
    pthread_t listening_thread;

};

#endif // TOUCHPAD_H
