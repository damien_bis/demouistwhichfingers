#ifndef EVTEST_H
#define EVTEST_H

typedef void (*touchCallback)(void *data,
                     long tvsec, long tvusec,
                     int x, int y,
                     int slot,
                     int removed);

typedef struct _listen_touchpad_params {
    int fd;
    touchCallback callback;
    void* data;
    int stop;
} listen_touchpad_params;

int get_device(unsigned short vendorId, unsigned short productId);
void get_touchpad_info(int fd, int* xmin, int* xmax, int* ymin, int* ymax);
int listen_touchpad_events(listen_touchpad_params* params);

#endif // EVTEST_H

