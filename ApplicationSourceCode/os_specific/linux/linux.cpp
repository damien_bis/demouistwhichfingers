#include "os_specific/os_specific.h"
#include "os_specific/linux/touchpad/touchpad.h"

Touchpad* touchpad;

void installTouchpadCallback() {
    touchpad = new Touchpad(0x2, 0xe);
    touchpad->startListening(onTouchpadEvent);    
}

void removeTouchpadCallback() {
    touchpad->stopListening();
    delete touchpad;
}
